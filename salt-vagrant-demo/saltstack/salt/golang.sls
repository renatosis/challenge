install_golang:
  pkg.installed:
    - name: golang-1.9-go
  environ.setenv:
    - name: GOPATH
    - value: /opt/gopath
  cmd.run:
    - names: 
      - ln -s /usr/lib/go-1.9/bin/go /usr/bin/go || true
      - cd /opt/gopath/src/gowebapp && go get .
      - cd /opt/gopath/src/gowebapp && go install
      - /opt/gopath/bin/gowebapp

  
