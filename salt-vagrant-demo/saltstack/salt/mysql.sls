install_mysql:
  pkg.installed:
    - name: mysql-server
  cmd.run:
    - name: if [ ! -f /opt/runonce ]; then /opt/gopath/src/gowebapp/mysql.sh ; touch /opt/runonce; fi
