# Step-by-step

## Understanding Saltstack:

I got this doc searching for saltstack on google:
https://docs.saltstack.com/en/getstarted/config/functions.html

## Installing Saltstack

So I had to install the Salt Vagrant Fundamentals:
Reference: https://docs.saltstack.com/en/getstarted/fundamentals/
git clone https://github.com/UtahDave/salt-vagrant-demo.git

I removed vagrant minion2 from Vagrantfile for this challenge

After vagranting up, its time to install saltstack:
sudo -i 
salt-key --list-all

All the keys were already accepted. =/
root@saltmaster:~# salt-key --list-all
Accepted Keys:
minion1
Denied Keys:
Unaccepted Keys:
Rejected Keys:

Its time to test it:
salt '*' test.ping

## Creating SaltStates

After test we have to create the saltstate for Mysql: mysql.sls
cat <<< EOF > saltstack/salt/mysql.sls
install_mysql:
  pkg.installed:
    - name: mysql-server
EOF

Run the salt command to apply states:
salt '*' state.apply

## Portforwarding 3306 and 80 host port to the guest vm
Reference: https://www.vagrantup.com/docs/networking/forwarded_ports.html

Then I modified VagrantFile to portforward the 3306 and 80 port to the guest vm:
minion_config.vm.network "forwarded_port", guest: 3306, host: 3306
minion_config.vm.network "forwarded_port", guest: 80, host: 80

After restarting the minion1 I could see the 3306 port up in my host
netstat -an | grep LISTEN | grep 3306
tcp4       0      0  *.3306                 *.*                    LISTEN

## Map the gowebapp folder to minions vm

I have to map the webapp folder to the guest minion1 vm on Vagrantfile before importing mysql.sql:
minion_config.vm.synced_folder "../gowebapp/", "/opt/gopath/src/gowebapp"

## Restart vagrant to map the gowebapp folder and create the mysql database structure

## Creating the first user to mysql

I had to change the Mysql SaltState to create the user, run import the sql structure and grant permissions:
To do that Ive create a bash script called mysql.sh:

#!/bin/bash
mysql -e "CREATE USER 'challenge'@'%' IDENTIFIED BY 'password';"
mysql < /opt/gowebapp/config/mysql.sql
mysql -e "GRANT ALL ON gowebapp.* TO 'challange'@'%';"

And changed saltstate to run this shell script:
if [ ! -f /opt/runonce ]; then /opt/gopath/src/gowebapp/mysql.sh ; touch /opt/runonce; fi

Run the salt command to apply states:
salt '*' state.apply

TODO: Search for another way to runonce a command in salt 

## Understanding the Go webapp

I've read the webapp's readme and I understand how to build the webapp and create the mysql structure

## I had a problem runnning this webapp on golang version 1.6 
go-sql-driver/mysql/utils.go:81: undefined: cloneTLSConfig

So I had to install the latest go version
- name: golang-1.9-go

I created a symlink to run go 1.9 commands as go
  cmd.run:
    - name: ln -s /usr/lib/go-1.9/bin/go /usr/bin/go || true

## Installing Go dependencies and install the app

To get the dependencies and install the app I had to change the golang.sls as follow:

  environ.setenv:
    - name: GOPATH
    - value: /opt/gopath
  cmd.run:
    - names: 
      - ln -s /usr/lib/go-1.9/bin/go /usr/bin/go || true
      - cd /opt/gopath/src/gowebapp && go get .
      - cd /opt/gopath/src/gowebapp && go install
      - /opt/gopath/bin/gowebapp



