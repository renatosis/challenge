#!/bin/bash

mysql -e "CREATE USER 'challenge'@'%' IDENTIFIED BY 'password';"
mysql < /opt/gopath/src/gowebapp/config/mysql.sql
mysql -e "GRANT ALL ON gowebapp.* TO 'challenge'@'%';"
mysql -e "FLUSH PRIVILEGES;"
